module CollectionParser

require 'json'

class Parser

def self.parse_file file
    data_file = File.read file
    data = JSON.parse(data_file)
    temp = {}
    temp = data["included"].select{|t| t["type"] == "products"}.last
    product_name = temp["attributes"]["name"]
    puts "Email ID : #{data["data"]["attributes"]["email-address"]}"
    puts "Phone Number : #{data["data"]["attributes"]["contact-number"]}"
    puts "Title : #{data["data"]["attributes"]["title"]} #{data["data"]["attributes"]["first-name"]}"
    puts "Product Name : #{product_name}"
end

end

end
